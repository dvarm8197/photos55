/**
 * controller contains all the controllers for the views
 */
package controller;

import application.Photo;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import application.Tag;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;

/**
 * Controller for tag view
 * @author Adit Patel and Daniel Varma
 */
public class TagViewController  {

    @FXML
    ScrollPane sp;
    Photo p;
    String albumName;

    @FXML Button backB;

    /**
     * sets album name
     * @param s name of album
     */
    public void setAlbumName(String s){ albumName = s;}

    /**
     * initalizes view with current tags for photo
     */
    public void initialize() {

        Platform.runLater(() -> {
            populateTags();
        //System.out.println(p.getName());

        });

    }

    /**
     *
     * @param photo photo to be edited
     */
    public void setPhoto(Photo photo){

        p = photo;
    }

    private String tagValuePop(){
        //System.out.println("In tag value pop");
        TextInputDialog dialog = new TextInputDialog("Enter name of tag");
        dialog.setTitle("Add tag value");
        dialog.setHeaderText("Tag Value");
        dialog.setContentText("Please enter tag value:");

        Optional<String> result = dialog.showAndWait();
        String tagValue = null;
        if (result.isPresent()){
            tagValue = result.get();
        } else {
            Alert er = new Alert(Alert.AlertType.ERROR);
            er.setTitle("Must enter something, try again");
            er.show();
            return null;
        }

        return tagValue;
    }


    /**
     * add new tag
     */
    @FXML public void addTag(){
       // ArrayList<String> c = LoginViewController.currentUser.getTagTypes();
        ArrayList<String> choices =LoginViewController.currentUser.getTagTypes(); //p.getTagTypes();
        //choices.add(0,"Add new tag type");

        ChoiceDialog<String> dialog = new ChoiceDialog<>("Choose one:", choices);
        dialog.setTitle("Choose tag");
        dialog.setHeaderText("Choose the tag type you want to add:");
        //dialog.setContentText("Choose your tag");

        Optional<String> result = dialog.showAndWait();
        String tagType = "";
        if (result.isPresent()){
            tagType = result.get();
        } else {
            //cancel or nothing pressed
            /*Alert er = new Alert(Alert.AlertType.ERROR);
            er.setTitle("Must enter something, try again");
            er.show();*/
            return;
        }

        ArrayList<Tag> tags = p.getTags();

        if (tagType.equals("Choose one:")){
            Alert er = new Alert(Alert.AlertType.ERROR);
            er.setTitle("Must enter something, try again");
            er.show();
            return;
        }
        else if(tagType.equals("Add new tag type")){
            TextInputDialog tagD = new TextInputDialog("Enter name of new tag type");
            tagD.setTitle("Add tag type");
            tagD.setHeaderText("Tag Type");
            tagD.setContentText("Please enter new tag type:");


            Optional<String> k = tagD.showAndWait();
            String newTag = null;
            if (k.isPresent()){
                newTag = k.get();
            } else {
                Alert er = new Alert(Alert.AlertType.ERROR);
                er.setTitle("Must enter something, try again");
                er.show();
                return;
            }
            LoginViewController.currentUser.addTagType(newTag);
            //choices.add(newTag);


            String tagValue = tagValuePop();

            Tag toAdd = new Tag();
            toAdd.setTagName(newTag);
            toAdd.setTagValue(tagValue);
            p.addTag(toAdd);
        }

        else if (tagType.equals("Location") && !tags.isEmpty()){
            //System.out.println("In location and tags not null");
            //only 1 location tag per pic
            for (Tag t:tags){
                if (t.getTagName().equals("Location")){
                    //error, try again
                    Alert er = new Alert(Alert.AlertType.ERROR);
                    er.setTitle("Location tag already exists for this picture");
                    er.show();
                    return;
                }
            }

            String tagValue = tagValuePop();

            Tag toAdd = new Tag();
            toAdd.setTagName("Location");
            toAdd.setTagValue(tagValue);
            p.addTag(toAdd);
        }

        else if (tags.isEmpty() && !tagType.equals("Add new tag type")){
            //System.out.println("tags is null");
            String tagValue = tagValuePop();

            Tag toAdd = new Tag();
            toAdd.setTagName(tagType);
            toAdd.setTagValue(tagValue);
            if (duplicate(toAdd)){
                Alert er = new Alert(Alert.AlertType.ERROR);
                er.setTitle("Duplicate tag for this photo");
                er.setContentText("Duplicate");
                er.show();
                return;
            }
            p.addTag(toAdd);


        }

        else if (!tags.isEmpty() ){
            //System.out.println("Tags not empty");
            String tagValue = tagValuePop();
            Tag toAdd = new Tag();
            toAdd.setTagName(tagType);
            toAdd.setTagValue(tagValue);
            if (duplicate(toAdd)){
                Alert er = new Alert(Alert.AlertType.ERROR);
                er.setTitle("Duplicate tag for this photo");
                er.setContentText("Duplicate");
                er.show();
                return;
            }
            p.addTag(toAdd);

        }

        populateTags();

       LoginViewController.currentUser.getAlbum(albumName).setPhoto(p);
        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    /**
     * check for duplicate tag
     * @param t tag to be checked
     * @return true if duplicate exists, false otherwise
     */
    public boolean duplicate(Tag t){
        ArrayList<Tag> ts = p.getTags();
        for (Tag i : ts){
            if (t.getTagValue().equalsIgnoreCase(i.getTagValue()) && t.getTagName().equalsIgnoreCase(i.getTagName())){
                return true;
            }
        }
        return false;
    }


    /**
     * populates list of tags
     * @return all the tags in a list
     */
    public ArrayList<Tag> populateTags(){
        VBox v = new VBox();
        //System.out.println("Populating tags");
        ArrayList<Tag> tags = p.getTags();

        if (tags.isEmpty()){
            v.getChildren().removeAll();
            sp.setContent(v);
            return null;
        }

        ArrayList<Tag> temp = tags;
        Collections.sort(temp, new Comparator<Tag>() {
            @Override
            public int compare(Tag o1, Tag o2) {
                return o1.getTagName().compareToIgnoreCase(o2.getTagName());
            }
        });

        for (Tag i: temp){
            //System.out.println(i.getTagName() + " " + i.getTagValue());
            TitledPane tp = new TitledPane();
            tp.setExpanded(false);
            tp.setId(i.getTagName());
            tp.setText(i.getTagName() + ": " + i.getTagValue());
            v.getChildren().add(tp);
        }
        sp.setContent(v);

        return temp;

    }

    /**
     * delete a tag
     */
    @FXML public void deleteTag(){
        ArrayList<Tag> tags = populateTags();
        TextInputDialog dialog = new TextInputDialog("Enter name of tag you want to delete");
        dialog.setTitle("Delete tag");
        dialog.setHeaderText("Enter tag name you want to delete");
        dialog.setContentText("Please enter tag name:");

        Optional<String> result = dialog.showAndWait();


        String tagName = null;
        if (result.isPresent()) {
            tagName = result.get();//result.ifPresent(name -> {return name;}
        } else {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("Must enter tag name");
            a.showAndWait();
            return;
        }

        TextInputDialog dialog2 = new TextInputDialog("Enter value of tag you want to delete");
        dialog2.setTitle("Delete tag");
        dialog2.setHeaderText("Enter tag value you want to delete");
        dialog2.setContentText("Please enter tag value:");

        Optional<String> result2 = dialog2.showAndWait();


        String tagValue = null;
        if (result2.isPresent()) {
            tagValue = result2.get();//result.ifPresent(name -> {return name;}
        } else {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("Must enter tag value");
            a.showAndWait();
            return;
        }

        int i = 0;
        Tag toDelete = null;
        for (Tag t: tags){
            if (t.getTagName().equals(tagName) && t.getTagValue().equals(tagValue)){
                toDelete = t;
            }
            i++;
        }

        if (toDelete == null){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("Tag name/value combination not found, try again");
            a.showAndWait();
            return;
        }

        tags.remove(toDelete);
        p.setTags(tags);
        populateTags();

        LoginViewController.currentUser.getAlbum(albumName).setPhoto(p);
        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * go back to previous viw
     */
    @FXML public void back(){
        Stage stage = (Stage) backB.getScene().getWindow();
        stage.close();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/PhotosDisplayView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            PhotosDisplayViewController pdv = fxmlLoader.getController();
            pdv.setAlbumName(albumName);

            Stage stages = new Stage();
            stages.setScene(new Scene(root1));
            stages.setTitle(albumName);
            stages.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
