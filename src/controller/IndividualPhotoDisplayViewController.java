package controller;

import application.Photo;
import application.Tag;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Controller to open 1 individual photo
 */
public class IndividualPhotoDisplayViewController {
    @FXML
    ImageView iview;

    @FXML
    Label idate;

    @FXML
    ScrollPane sp;
    //need date
    Photo p;
    String albumName;
    /**
     * Initalizes view with picture, date, and tags
     */
    public void initialize() {

        Platform.runLater(() -> {
            setPic();
            setDate();
            setTags();
        });

    }

    /**
     * helper function to set the tags
     */
    public void setTags(){
        VBox v = new VBox();
        //System.out.println("Populating tags");
        ArrayList<Tag> tags = p.getTags();

        if (tags.isEmpty()){
            v.getChildren().removeAll();
            sp.setContent(v);
            return;
        }

        ArrayList<Tag> temp = tags;
        Collections.sort(temp, new Comparator<Tag>() {
            @Override
            public int compare(Tag o1, Tag o2) {
                return o1.getTagName().compareToIgnoreCase(o2.getTagName());
            }
        });

        for (Tag i: temp){
            //System.out.println(i.getTagName() + " " + i.getTagValue());
            TitledPane tp = new TitledPane();
            tp.setExpanded(false);
            tp.setId(i.getTagName());
            tp.setText(i.getTagName() + ": " + i.getTagValue());
            v.getChildren().add(tp);
        }
        sp.setContent(v);
    }

    /**
     * helper function to set the date
     */
    public void setDate(){
        Calendar cal = p.getDate();
        Date date = cal.getTime();
        //String s = d.toString();
       // DateTimeFormatter formmat1 = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);

        SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");
       // ParseException
        String inActiveDate = null;
        try {
            inActiveDate = format1.format(date);
            //System.out.println(inActiveDate );
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        //String formatted = format1.format(date);

        idate.setText(inActiveDate);
    }

    /**
     * helper function to set the picture
     */
    public void setPic(){
        try {
            FileInputStream fis = new FileInputStream(p.getPath());
            Image i = new Image(fis);

            iview.setImage(i);
            iview.setId(p.getName());
            //ImageView iv = new ImageView(i);
            //iv.setId(p.getName());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param photo set the photo
     */
    public void setPhoto(Photo photo){
        p = photo;
    }

    /**
     *
     * @param s set name of the album
     */
    public void setAlbumName(String s){
        albumName = s;
    }

    /**
     * to go back to previous view
     */
    public void back(){
        Stage stage = (Stage) iview.getScene().getWindow();
        stage.close();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/PhotosDisplayView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            PhotosDisplayViewController pdv = fxmlLoader.getController();
            pdv.setAlbumName(albumName);

            Stage stages = new Stage();
            stages.setScene(new Scene(root1));
            stages.setTitle(albumName);
            stages.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
