package controller;

import application.Album;
import application.Photo;
import application.Tag;
import application.User;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.time.LocalDate;
import java.util.*;

/**
 * search view controller
 * @author Adit Patel and Daniel Varma
 */
public class SearchViewController {

    @FXML Button back,create,date,tag;
    String albumName;


    @FXML
    FlowPane fp;
    @FXML
    ScrollPane sp;

    @FXML DatePicker date1, date2;
    @FXML ChoiceBox singleTagName, singleTagValue, multiTagName1, multiTagValue1, multiTagName2, multiTagValue2, combo;


    ArrayList<Photo> newPhoto = new ArrayList<Photo>();


    /**
     * initializes view with photos
     */
    public void initialize() {

        Platform.runLater(() -> {
            //need to populate dropdowns
            List<String> tagName = new ArrayList<String>();
            List<String> com = new ArrayList<String>();
            com.add("and"); com.add("or");
            for(Album a : LoginViewController.currentUser.getAlbums()) {
                //if(a.getName().equals(albumName)) {
                    for(Photo p : a.getPhotos()) {
                       for(Tag t : p.getTags()){
                           tagName.add(t.getTagName());
                       }
                    }
                //}
            }
            Set<String> hs = new HashSet<>();
            hs.addAll(tagName);
            tagName.clear();
            tagName.addAll(hs);
            singleTagName.setItems(FXCollections.observableArrayList(tagName));
            multiTagName1.setItems(FXCollections.observableArrayList(tagName));
            multiTagName2.setItems(FXCollections.observableArrayList(tagName));
            combo.setItems(FXCollections.observableArrayList(com));
        });
    }

    //given the album name, photos are displayed on the UI
    public void populatePhotos(String albumName){
        //System.out.println("Populating photos");
        for(Album a : LoginViewController.currentUser.getAlbums()){
            System.out.println(a.getName() +","+albumName);
            if(a.getName().equals(albumName)){

                if(a.getPhotos().isEmpty()){
                    //System.out.println("empty album");
                    return;
                }

                for(Photo p : a.getPhotos()){
                    try {
                        //System.out.println("found it");
                        FileInputStream f = new FileInputStream(p.getPath());
                        String cap = p.getCaption();
                        Label l = new Label(cap);


                        Image i = new Image(f);
                        ImageView iv = new ImageView(i);
                        iv.setId(p.getName());
                        iv.setFitHeight(200);
                        iv.setFitWidth(200);
                        l.setGraphic(iv);


                        fp.getChildren().add(l);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
                break;
            }
        }
    }

    public void setAlbumName(String album){
        albumName = album;
    }


    /**
     * create the album
     */
    @FXML
    public void createAlbum(){
        if(newPhoto.isEmpty()){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("No photos to create album");
            a.showAndWait();
            return;
        }

        try {
            AdminViewController.users = AdminViewController.readUser();
        } catch(Exception e){
            e.printStackTrace();
        }

        //ask for album name
        TextInputDialog dialog = new TextInputDialog("Enter album name");
        dialog.setTitle("Create Album");
        dialog.setHeaderText("Album");
        dialog.setContentText("Please enter album name:");

        Optional<String> result = dialog.showAndWait();

        if(!result.isPresent()){
            //user cancelled so return
            return;
        }

        String albumName = null;
        if (result.isPresent()) {
            albumName = result.get();//result.ifPresent(name -> {return name;}
        }

        //make sure there is no duplicate
        if(dupAlbum(albumName, LoginViewController.currentUser)){
            //System.out.println("Dup album name");
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Duplicate Album name");
            a.setContentText("Album name already exists, try again");
            a.showAndWait();
            return;
        }


        //create the album, add to user list
        Album a = new Album(albumName);
        a.setPhotos(newPhoto);
        a.findMin();
        a.findMax();
        LoginViewController.currentUser.getAlbums().add(a);
        for(User u : AdminViewController.users){
            if(u.getUserName().equals(LoginViewController.currentUser.getUserName())){
                u.setAlbums(LoginViewController.currentUser.getAlbums());
                break;
            }
        }

        //serialize
        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    /**
     * checks for duplicate album
     * @param albumName album to be checked
     * @param u current user
     * @return true if duplicate exists, false otherwise
     */
    public boolean dupAlbum(String albumName, User u){
        for(Album a : u.getAlbums()){
            if(a.getName().equals(albumName)){
                return true;
            }
        }
        return false;
    }

    /**
     * populate view with photos
     */
    public void populatePhotos(){
        //adding to the UI
        for(Photo p : newPhoto){
            try {
                //System.out.println("found it");
                FileInputStream f = new FileInputStream(p.getPath());
                String cap = p.getCaption();
                Label l = new Label(cap);
                Image i = new Image(f);
                ImageView iv = new ImageView(i);
                iv.setId(p.getName());
                iv.setFitHeight(200);
                iv.setFitWidth(200);
                l.setGraphic(iv);
                fp.getChildren().add(l);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }


    /**
     * search with date function
     */
    @FXML
    public void searchDate(){
        //check if dates are not null
        newPhoto = new ArrayList<Photo>();
        fp.getChildren().clear();
        if(date1.getValue() == null || date2.getValue() == null){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("Please select dates");
            a.showAndWait();
            return;
        }

        //date 1 can't be after date 2
        if (date1.getValue().isAfter(date2.getValue())){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("First date can't be after second date");
            a.showAndWait();
            return;
        }

        //look through photos in album looking to see if its in the data range

        for(Album a : LoginViewController.currentUser.getAlbums()) { //get dates of the photos in album
            if (a.getPhotos().isEmpty()) {
               // System.out.println("empty album");
                continue;
            }

            //seeing if its in the date range
            for (Photo p: a.getPhotos()){
                Calendar c = p.getDate();
                LocalDate photoDate = c.toInstant()
                        .atZone(c.getTimeZone().toZoneId())
                        .toLocalDate();
                if(photoDate.isAfter(date1.getValue()) && photoDate.isBefore(date2.getValue())){
                    //add photo to list
                    newPhoto.add(p);
                }
            }

        }
        populatePhotos();
        //System.out.println(date1.getValue().toString()+","+ date2.getValue().toString());
    }

    /**
     * serach with a single tag
     */
    @FXML
    public void searchSingleTag(){

        newPhoto = new ArrayList<Photo>();
        fp.getChildren().clear();
        if(singleTagName.getValue() == null || singleTagValue.getValue() == null){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("Please select tag name and value");
            a.showAndWait();
            return;
        }

        for(Album a : LoginViewController.currentUser.getAlbums()) {
            for(Photo p : a.getPhotos()) {
                for(Tag t : p.getTags()){
                    //System.out.println(t.getTagName()+","+singleTagName.getValue()+","+t.getTagValue()+","+singleTagValue.getValue());
                    if(t.getTagName().equals(singleTagName.getValue())&&t.getTagValue().equals(singleTagValue.getValue())){
                       // System.out.println("adding");
                        Photo deepCopy = new Photo(p.getName(),p.getDate(),p.getPath());
                        deepCopy.setCaption(p.getCaption());
                        p.setTags(p.getTags());
                        newPhoto.add(p);
                    }
                }
            }

        }
        populatePhotos();

    }

    /**
     * Helper function for single tag search
     */
    @FXML
    public void fillSingle(){
        //System.out.println("fill single");
        List<String> tagVal = new ArrayList<String>();
        for(Album a : LoginViewController.currentUser.getAlbums()) {
            for(Photo p : a.getPhotos()) {
                for(Tag t : p.getTags()){
                    if(t.getTagName().equals(singleTagName.getValue())){
                        tagVal.add(t.getTagValue());
                    }
                }
            }

        }
        Set<String> hs = new HashSet<>();
        hs.addAll(tagVal);
        tagVal.clear();
        tagVal.addAll(hs);
        singleTagValue.setItems(FXCollections.observableArrayList(tagVal));

    }

    /**
     * helper function for multi tag serach for 1st tag
     */
    @FXML
    public void fillMulti1(){
        //System.out.println("fill multi1");
        List<String> tagVal = new ArrayList<String>();
        for(Album a : LoginViewController.currentUser.getAlbums()) {
            for(Photo p : a.getPhotos()) {
                for(Tag t : p.getTags()){
                    if(t.getTagName().equals(multiTagName1.getValue())){
                        //System.out.println("found");
                        tagVal.add(t.getTagValue());
                    }
                }
            }

        }
        Set<String> hs = new HashSet<>();
        hs.addAll(tagVal);
        tagVal.clear();
        tagVal.addAll(hs);
        multiTagValue1.setItems(FXCollections.observableArrayList(tagVal));
    }

    /**
     * helper function with multi tag for 2nd tag
     */
    @FXML
    public void fillMulti2(){
        //System.out.println("fill multi2");
        List<String> tagVal = new ArrayList<String>();
        for(Album a : LoginViewController.currentUser.getAlbums()) {
            for(Photo p : a.getPhotos()) {
                for(Tag t : p.getTags()){
                    if(t.getTagName().equals(multiTagName2.getValue())){
                        tagVal.add(t.getTagValue());
                    }
                }
            }

        }
        Set<String> hs = new HashSet<>();
        hs.addAll(tagVal);
        tagVal.clear();
        tagVal.addAll(hs);
        multiTagValue2.setItems(FXCollections.observableArrayList(tagVal));
    }

    /**
     * multi tag search
     */
    @FXML
    public void searchMultiTag(){
        newPhoto = new ArrayList<Photo>();
        fp.getChildren().clear();

        if(multiTagName1.getValue() == null || multiTagName2.getValue() == null ||
                multiTagValue1.getValue() == null || multiTagValue2.getValue() == null || combo.getValue() == null){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("Please select tag names,values, and conjunction/disjunction");
            a.showAndWait();
            return;
        }
        String con = (String)combo.getValue();
        for(Album a : LoginViewController.currentUser.getAlbums()) {
            for(Photo p : a.getPhotos()) {
                for(Tag t : p.getTags()){
                    if(con.equals("and")) {
                        if ((t.getTagName().equals(multiTagName1.getValue()) && t.getTagValue().equals(multiTagValue1.getValue()))) {
                            for (Tag t2 : p.getTags()) {
                                if ((t2.getTagName().equals(multiTagName1.getValue()) && t2.getTagValue().equals(multiTagValue1.getValue()))) {
                                    newPhoto.add(p);
                                }
                            }
                        }
                    }else if(con.equals("or")) {
                        if ((t.getTagName().equals(multiTagName1.getValue()) && t.getTagValue().equals(multiTagValue1.getValue()))
                                ||(t.getTagName().equals(multiTagName2.getValue()) && t.getTagValue().equals(multiTagValue2.getValue()))) {
                            //System.out.println("adding");
                            newPhoto.add(p);
                        }
                    }


                }


            }

        }

        populatePhotos();
    }


    /**
     * to go back to previous view
     */
    @FXML
    public void back(){
        Stage stage = (Stage) back.getScene().getWindow();
        stage.close();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/AlbumView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            //PhotosDisplayViewController pd = fxmlLoader.getController();
           // pd.setAlbumName(albumName);
            Stage stages = new Stage();
            stages.setScene(new Scene(root1));
            stages.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }


}
