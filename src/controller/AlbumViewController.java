package controller;

import application.Album;
import application.User;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * Album view controller that lists the albums and methods for them
 */
public class AlbumViewController {

    @FXML  ScrollPane albumList;
    @FXML Button logoutB;

    /**
     * populates albums for current user
     */
    public void initialize(){
        populateAlbums(LoginViewController.currentUser, "../view/PhotosDisplayView.fxml" );
    }

    /**
     * search function
     */
    @FXML
    public void search(){
        try {
            Stage clo = (Stage) logoutB.getScene().getWindow();
            clo.close();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/SearchView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            SearchViewController sv = fxmlLoader.getController();
            //sv.setAlbumName(albumName);
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Search");
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * to make a new album
     */
    @FXML
    public void createAlbum(){
        try {
            AdminViewController.users = AdminViewController.readUser();
        } catch(Exception e){
            e.printStackTrace();
        }

        //ask for album name
        TextInputDialog dialog = new TextInputDialog("Enter album name");
        dialog.setTitle("Create Album");
        dialog.setHeaderText("Album");
        dialog.setContentText("Please enter album name:");

        Optional<String> result = dialog.showAndWait();

        if(!result.isPresent()){
            //user cancelled so return
            return;
        }

        String albumName = null;
        if (result.isPresent()) {
            albumName = result.get();//result.ifPresent(name -> {return name;}
        }

        //make sure there is no duplicate
        if(dupAlbum(albumName, LoginViewController.currentUser)){
            //System.out.println("Dup album name");
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Duplicate Album name");
            a.setContentText("Album name already exists, try again");
            a.showAndWait();
            return;
        }


        //create the album, add to user list
        Album a = new Album(albumName);
        LoginViewController.currentUser.getAlbums().add(a);
        for(User u : AdminViewController.users){
            if(u.getUserName().equals(LoginViewController.currentUser.getUserName())){
                u.setAlbums(LoginViewController.currentUser.getAlbums());
                break;
            }
        }

        populateAlbums(LoginViewController.currentUser, "../view/PhotosDisplayView.fxml");
        //serialize
        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    /**
     * Checks to see if duplicate album exists
     * @param albumName name of the album
     * @param u  user
     * @return true if it exists, false otherwise
     */
    //checks if there that album name is already in the list
    public boolean dupAlbum(String albumName, User u){
        for(Album a : u.getAlbums()){
            if(a.getName().equals(albumName)){
                return true;
            }
        }
        return false;
    }

    /**
     * Helper function to initalize
     * @param u current user
     * @param view Path of the view
     */
    //lists albums in the UI
    public void populateAlbums(User u, String view){
        VBox v = new VBox();
        if(u.getAlbums().isEmpty()){
            //System.out.println("empty list");
            v.getChildren().removeAll();
            albumList.setContent(v);
            return;
        }
        for(Album album : u.getAlbums()) {
            //System.out.println(album.getName());
            TitledPane tp = new TitledPane();
            tp.setExpanded(false);
            tp.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    try {

                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(view));
                        Parent root1 = (Parent) fxmlLoader.load();
                        PhotosDisplayViewController c = fxmlLoader.getController();
                        TitledPane t = (TitledPane)event.getSource();
                        c.setAlbumName(album.getName());
                        Stage stage = new Stage();
                        stage.setScene(new Scene(root1));
                        Stage clo = (Stage) albumList.getScene().getWindow();
                        clo.close();
                        stage.setTitle(album.getName());
                        stage.show();
                        return;
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });
            tp.setId(album.getName());
            String d1 = setDate(album.getMinDate());
            String d2 = setDate(album.getMaxDate());
            tp.setText(album.getName() +"  Photos: "+album.getPhotos().size() +" Date: "+d1+"-"+d2);
            v.getChildren().add(tp);

        }
        albumList.setContent(v);
    }

    /**
     *
     * @param c new date
     * @return string format of date
     */
    public String setDate(Calendar c){
        if(c == null){
            return "";
        }
        Date date = c.getTime();

        SimpleDateFormat format1 = new SimpleDateFormat("MM-dd-yyyy");
        // ParseException
        String inActiveDate = null;
        try {
            inActiveDate = format1.format(date);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        //String formatted = format1.format(date);

        return  inActiveDate;
    }


    /**
     * to delete album for current user
     */
    @FXML
    public void deleteAlbum(){
        try {
            AdminViewController.users = AdminViewController.readUser();
        } catch(Exception e){
            e.printStackTrace();
        }

        //ask for album name to be deleted
        TextInputDialog dialog = new TextInputDialog("Enter album name that you want to delete");
        dialog.setTitle("Delete Album");
        dialog.setHeaderText("Album");
        dialog.setContentText("Please enter name of album you want to delete:");

        Optional<String> result = dialog.showAndWait();



        ArrayList<Album> albums = LoginViewController.currentUser.getAlbums();

        Album toDelete = null;
        //System.out.println();
        for (Album i: albums){
            //System.out.println(i.getName());
            if (result.get().equals(i.getName())){
                //deleting this album
                //System.out.println("Deleting: " + result);
                toDelete = i;

            }
        }

        if (toDelete == null){
            //no match found
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("No Album with that name exists for this user, try again");
            a.showAndWait();
            return;
        }

        albums.remove(toDelete);
        LoginViewController.currentUser.setAlbums(albums);
        for(User u : AdminViewController.users){
            if(u.getUserName().equals(LoginViewController.currentUser.getUserName())){
                u.setAlbums(LoginViewController.currentUser.getAlbums());
                break;
            }
        }

        populateAlbums(LoginViewController.currentUser,"../view/PhotosDisplayView.fxml");
        //serialize
        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    /**
     * to rename an album
     */
    @FXML
    public void renameAlbum(){
        try {
            AdminViewController.users = AdminViewController.readUser();
        } catch(Exception e){
            e.printStackTrace();
        }

        //ask for album name to be renamed
        TextInputDialog dialog = new TextInputDialog("Enter album name that you want to rename");
        dialog.setTitle("Rename Album");
        dialog.setHeaderText("Album");
        dialog.setContentText("Please enter name of album you want to rename:");

        Optional<String> result = dialog.showAndWait();

        TextInputDialog dialog2 = new TextInputDialog("Enter new name");
        dialog2.setTitle("Rename Album");
        dialog2.setHeaderText("Album");
        dialog2.setContentText("Please enter new name of album");

        Optional<String> renamed = dialog2.showAndWait();


        ArrayList<Album> albums = LoginViewController.currentUser.getAlbums();

        //make sure there is no duplicate
        if(dupAlbum(renamed.get(), LoginViewController.currentUser)){
            //System.out.println("Dup album name");
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Duplicate Album name");
            a.setContentText("Album name already exists, try again");
            a.showAndWait();
            return;
        }

        Album toRename = null;
        for (Album i: albums){
            if (result.get().equals(i.getName())){
                //renaming this album
               // System.out.println("Deleting: " + result);
                toRename = i;
                i.setName(renamed.get());

            }
        }

        if (toRename == null){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("No Album with that name exists for this user, try again");
            a.showAndWait();
            return;
        }


        LoginViewController.currentUser.setAlbums(albums);
        for(User u : AdminViewController.users){
            if(u.getUserName().equals(LoginViewController.currentUser.getUserName())){
                u.setAlbums(LoginViewController.currentUser.getAlbums());
                break;
            }
        }

        populateAlbums(LoginViewController.currentUser,"../view/PhotosDisplayView.fxml");
        //serialize
        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    /**
     * to logout and go back to login view
     */
    @FXML
    public void logout(){
        Stage stage = (Stage) logoutB.getScene().getWindow();
        stage.close();


        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/LoginView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stages = new Stage();
            stages.setScene(new Scene(root1));
            stages.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
