package controller;

import application.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * controller for login. Where the application starts
 */
public class LoginViewController {

    @FXML Button login;
    @FXML TextField userName;
    @FXML ScrollPane albumList;
    public static User currentUser;

    /**
     * Login to check if user exists
     */
    @FXML
    public void login(){
        String uName = userName.getText();
        //System.out.println(uName);
        if (uName.equalsIgnoreCase("stock")){

            try {
                AdminViewController.users = AdminViewController.readUser();
                //System.out.println("Successful read user");
            }catch(Exception e){
                e.printStackTrace();
            }

            for (User u: AdminViewController.users){
                if(uName.equals("stock")){
                    Stage clo = (Stage) login.getScene().getWindow();
                    clo.close();
                    //found user go to album view
                    try {
                        currentUser = u;
                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/AlbumView.fxml"));
                        Parent root1 = (Parent) fxmlLoader.load();
                        Stage stage = new Stage();
                        stage.setScene(new Scene(root1));
                        stage.show();
                        return;
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(uName.equals("admin")){
            Stage clo = (Stage) login.getScene().getWindow();
            clo.close();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/AdminView.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root1));
                stage.show();
            }catch(Exception e){
                //System.out.println("Error");
                e.printStackTrace();
            }
        }else{
            //check if user exists
            try {
                AdminViewController.users = AdminViewController.readUser();
                //System.out.println("Successful read user");
            }catch(Exception e){
                e.printStackTrace();
            }
            for(User u: AdminViewController.users){

                if(uName.equals(u.getUserName())){
                    Stage clo = (Stage) login.getScene().getWindow();
                    clo.close();
                    //found user go to album view
                    try {
                        currentUser = u;
                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/AlbumView.fxml"));
                        Parent root1 = (Parent) fxmlLoader.load();
                        Stage stage = new Stage();
                        stage.setScene(new Scene(root1));
                        stage.show();
                        return;
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
            //could not find user
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("User does not exist");
            a.showAndWait();
            return;
        }
    }






}
