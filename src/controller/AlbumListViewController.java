package controller;

import application.Album;
import application.Photo;
import application.User;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * AlbumList view
 */
public class AlbumListViewController {

    boolean copy = false;
    boolean move = false;

    @FXML ScrollPane albums;
    @FXML Button back;
    Photo p;
    String sourceAlbum;

    /**
     * Initializes screen with current albums of current user
     */
    public void initialize(){

        Platform.runLater(() -> {
            VBox v = new VBox();
            if (LoginViewController.currentUser.getAlbums().isEmpty()) {
                //System.out.println("empty list");
                v.getChildren().removeAll();
                albums.setContent(v);
                return;
            }
            for (Album album : LoginViewController.currentUser.getAlbums()) {
                //System.out.println(album.getName());
                if(album.getName().equals(sourceAlbum)){
                    continue;
                }
                TitledPane tp = new TitledPane();
                tp.setExpanded(false);
                tp.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        try {
                            //album to be moved or copy to
                            if(move){
                                copy_move_Photo("move",album.getName());
                            }else if (copy){
                                copy_move_Photo("copy", album.getName());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                tp.setId(album.getName());
                tp.setText(album.getName());
                v.getChildren().add(tp);

            }
            albums.setContent(v);
        });
    }

    /**
     * To copy or move a photo into another album
     * @param s Photo to be copied/moved
     * @param destAlbum Album to move it to
     */
    public void copy_move_Photo(String s, String destAlbum){

        //check if photo already exists in the album
        for(Album a : LoginViewController.currentUser.getAlbums()){
            if(a.getName().equals(destAlbum)){
                for(Photo photo : a.getPhotos()){
                    if(photo.getName().equals(p.getName())){
                        //same photo
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setContentText("Photo already exists in this album");
                        alert.showAndWait();
                        return;
                    }

                }
            }
        }

        if(s.equals("move")){
            //remove photo from source album
            for(Album a : LoginViewController.currentUser.getAlbums()){
                if(a.getName().equals(sourceAlbum)){
                    for(Photo photo : a.getPhotos()){
                        if(photo.getName().equals(p.getName())){
                            a.getPhotos().remove(p);
                            a.findMin();
                            a.findMax();
                            for(User u : AdminViewController.users){
                                if(u.getUserName().equals(LoginViewController.currentUser.getUserName())){
                                    u.setAlbums(LoginViewController.currentUser.getAlbums());
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        //copy it into dest album
        for(Album a : LoginViewController.currentUser.getAlbums()){
            if(a.getName().equals(destAlbum)){
                Photo deepPhoto = new Photo(p.getName(),p.getDate(),p.getPath());
                deepPhoto.setCaption(p.getCaption());
                deepPhoto.setTags(p.getTags());
                a.getPhotos().add(deepPhoto);
                a.findMax();
                a.findMin();
                for(User u : AdminViewController.users){
                    if(u.getUserName().equals(LoginViewController.currentUser.getUserName())){
                        u.setAlbums(LoginViewController.currentUser.getAlbums());
                        break;
                    }
                }
                break;

            }
        }

        //serialize
        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }


        //load back up photodisplay
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/PhotosDisplayView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            PhotosDisplayViewController c = fxmlLoader.getController();
            c.setAlbumName(sourceAlbum);
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            Stage clo = (Stage) albums.getScene().getWindow();
            clo.close();
            stage.setTitle(sourceAlbum);
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }


    }


    /**
     * helper function to check if copy or move
     * @param photo to be moved
     * @param s Copy or move
     * @param alb Album to move it to
     */
    public void setBool(Photo photo, String s, String alb){
        p = photo;
        sourceAlbum = alb;
        if(s.equals("copy")){
            //System.out.println("copying");
            copy = true;
            move = false;
        }else if(s.equals("move")){
            //System.out.println("moving");
            move = true;
            copy = false;
        }else{
            //System.out.println("fail in album list view");
        }
    }

    /**
     * To go back to previous view
     */
    @FXML public void back(){
        Stage stage = (Stage) back.getScene().getWindow();
        stage.close();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/PhotosDisplayView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            PhotosDisplayViewController pdv = fxmlLoader.getController();
            pdv.setAlbumName(sourceAlbum);
            Stage stages = new Stage();
            stages.setScene(new Scene(root1));
            stages.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }




}
