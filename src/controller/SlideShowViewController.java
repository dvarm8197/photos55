package controller;

import application.Photo;
import javafx.application.Platform;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import java.io.FileInputStream;
import java.util.ArrayList;
import javafx.scene.image.*;
import javafx.stage.Stage;

/**
 * Controller for slideshow view
 * @author Adit Patel and Daniel Varma
 */
public class SlideShowViewController {

    int index = 0;
    ArrayList<Photo> photos;
    String albumName;

    @FXML
    ImageView slideshow;

    /**
     * initalizes view with first photo in album
     */
    public void initialize() {

        Platform.runLater(() -> {
            index = 0;
            setPic();
        });

    }

    /**
     *
     * @param p set the list of photos in the album
     */
    public void setPhotoList(ArrayList<Photo> p){
        photos = p;
    }

    /**
     * set name of album
     * @param a name of album
     */
    public void setAlbumName(String a){
        albumName = a;
    }

    /**
     * sets the picture in the view
     */
    public void setPic(){
        Photo p = photos.get(index);
        try {
            FileInputStream fis = new FileInputStream(p.getPath());
            Image i = new Image(fis);

            slideshow.setImage(i);
            slideshow.setId(p.getName());
            //ImageView iv = new ImageView(i);
            //iv.setId(p.getName());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * next picture
     */
    @FXML
    public void next(){
        if (index == photos.size() - 1){
            //out of bounds
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("No next photo");
            a.showAndWait();
            return;
        }
        index++;
        setPic();
    }

    /**
     * previous picture
     */
    @FXML
    public void prev(){
        if (index == 0){
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("No previous photo");
            a.showAndWait();
            return;
        }
        index--;
        setPic();
    }

    /**
     * exit slideshow view
     */
    @FXML
    public void exit(){
        Stage stage = (Stage) slideshow.getScene().getWindow();
        stage.close();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/PhotosDisplayView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            PhotosDisplayViewController pdv = fxmlLoader.getController();
            pdv.setAlbumName(albumName);
            Stage stages = new Stage();
            stages.setScene(new Scene(root1));
            stages.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
