package controller;
import application.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;

//import view.*;

/**
 * Controller for admin view
 * @author Adit Patel and Daniel Varma
 */
public class AdminViewController implements Serializable {
    @FXML ScrollPane scrollpane;
    @FXML Button logoutB;

    public static final String storeDir = "data";
    public static final String storeFile = "users.ser";
    public static ArrayList<User>  users = new ArrayList<User>();


    /**
     * Serializes the list of users
     * @param u List of users
     * @throws IOException
     */
    public static void writeUser(ArrayList<User> u) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storeDir + File.separator + storeFile));
        oos.writeObject(u);
    }

    /**
     * Reads serialized file
     * @return List of users
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static ArrayList<User> readUser() throws IOException, ClassNotFoundException{
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(storeDir + File.separator + storeFile));
        ArrayList<User> users = (ArrayList<User>) ois.readObject();
        return users;
    }



    VBox box = new VBox();

    public void addScrollPane(String user){
        //VBox box = new VBox();
        //adding label to scrollpane
        Label l = new Label();
        l.setId(user);
        l.setText(user);
        //scrollpane.setContent(box);
        box.getChildren().add(l);
        scrollpane.setContent(box);
    }


    /**
     * creates the user
     */
    @FXML
    public void create(){
        TextInputDialog dialog = new TextInputDialog("Enter user name");
        dialog.setTitle("Admin create user");
        dialog.setHeaderText("Admin");
        dialog.setContentText("Please enter user name:");

        Optional<String> result = dialog.showAndWait();


        String username = null;
        if (result.isPresent()) {
            username = result.get();//result.ifPresent(name -> {return name;}
        } else {
            //System.out.println("Error");
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setContentText("Must enter name");
            a.showAndWait();
            return;
        }
        //System.out.println(username);

        //check to see if ser file already exists
        File ser = new File("data/users.ser");
        if (ser.exists()){
            //System.out.println("exists");
            //check if user already exists
            try {
                users = AdminViewController.readUser();
            //    System.out.println("Successful read user");
            }catch(Exception e){
                e.printStackTrace();
            }

            //iterating through users array to see
            for (User i: users){
                if (username.equals(i.getUserName())){
                   // System.out.println("Duplicate");
                    Alert a = new Alert(Alert.AlertType.ERROR);
                    a.setTitle("Error");
                    a.setContentText("User already exists, try again");
                    a.showAndWait();
                    return;
                }
            }

            User newUser = new User(username);
            users.add(newUser);
            //addScrollPane(username);
            try {
                writeUser(users);
            }catch (Exception e){
                e.printStackTrace();
            }


        } else { //ser doesnt exist, add and write user to it
            //System.out.println("doesnt exist");
            //create the user
            User newUser = new User(username);

           //addScrollPane(username);

            users.add(newUser);
            try {
                writeUser(users);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

    }


    /**
     * To view the users
     */
    @FXML
    public void view(){
        try {
            users = AdminViewController.readUser();
        } catch(Exception e){
            e.printStackTrace();
        }

        String shown = "";
        for (User i: users){
            shown += i.getUserName() + "\n";
        }

        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("List of users");
        a.setContentText(shown);
        a.show();


    }


    /**
     * To delete a user
     * @throws IOException
     */
    @FXML
    public void delete() throws IOException{
        TextInputDialog dialog = new TextInputDialog("Enter user name");
        dialog.setTitle("Admin create user");
        dialog.setHeaderText("Admin");
        dialog.setContentText("Please enter user name:");

        Optional<String> result = dialog.showAndWait();


        String username = null;
        if (result.isPresent()) {
            username = result.get();//result.ifPresent(name -> {return name;}
        }

        //System.out.println(username);

        try {
            users = AdminViewController.readUser();
        } catch(Exception e){
            e.printStackTrace();
        }

        User t = null ;
        for (User i: users){
            if (username.equals(i.getUserName())){
                t = i;
            }
        }

        if (t == null){
            //not found in list
            Alert er = new Alert(Alert.AlertType.ERROR);
            er.setTitle("User not found");
            er.show();
            return;
        } else {
            Alert er = new Alert (Alert.AlertType.CONFIRMATION);
            er.setTitle("Successfull delete");
            er.setContentText("Succesfully deleted");
            er.show();
        }

        users.remove(t);

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storeDir + File.separator + storeFile));
        oos.writeObject(users);

        /*try {
            AdminViewController.writeUser(users);
        }catch (Exception e){
            e.printStackTrace();
        }*/
    }

    /**
     * To logout of the admin account
     */
    @FXML
    public void logout() {
        Stage stage = (Stage) logoutB.getScene().getWindow();
        stage.close();


        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/LoginView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stages = new Stage();
            stages.setScene(new Scene(root1));
            stages.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
