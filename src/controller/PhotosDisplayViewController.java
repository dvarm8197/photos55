package controller;

import application.Album;
import application.Photo;
import application.User;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

/**
 * Controller for photo display view
 */
public class PhotosDisplayViewController {


    @FXML FlowPane photoList;
    @FXML Button remove, caption, display, search, tag, copy, move, slideshow, back;
    @FXML
    Label choice;
    String albumName;
    boolean rem = false; boolean cap = false; boolean disp = false; boolean searching = false;
    boolean tagging; boolean copying; boolean moving = false; boolean slide = false;

    /**
     * Initalizes view with photos
     */
    public void initialize() {

        Platform.runLater(() -> {
            rem = cap = disp = searching = tagging = copying = moving = slide = false;
            populatePhotos(albumName);
            //System.out.println("Albumname : " + albumName);
        });

    }

    /**
     *
     * @param s new album name
     */
    public void setAlbumName(String s){

        albumName = s;
    }

    /**
     *
     * @param e helper function to check what user picks
     */
    @FXML
    public void setBool(ActionEvent e){
        rem = cap = disp = searching = tagging = copying = moving = slide = false;
        Button b = (Button)e.getSource();
        //System.out.println(b.getText());
        if(b == remove){
            choice.setText("Select photos you want to remove");
            rem = true;
        }else if(b == caption){
            choice.setText("Select photos you want to caption");
            rem = false;
            cap = true;
        }else if(b == display){
            choice.setText("Select photos you want to display");
            rem = false;
            disp = true;
        }else if(b == tag){
            choice.setText("Select photos you want to tag");
            rem = false;
            tagging = true;
        }else if(b == copy){
            choice.setText("Select photos you want to copy");
            rem = false;
            copying = true;
        }else if(b == move){
            choice.setText("Select photos you want to move");
            rem = false;
            moving = true;
        }
    }

    /**
     * add photo
     */
    @FXML
    public void addPhoto(){
        FileChooser fc = new FileChooser();
        File select = fc.showOpenDialog(null);

        if(select != null){
            Calendar c = Calendar.getInstance();
            Date d = new Date(select.lastModified());
            c.setTime(d);
            c.set(Calendar.MILLISECOND,0);
            //check if file is an image
            try {
                if (ImageIO.read(select) == null) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("Not image file");
                    alert.showAndWait();
                    //System.out.println("That is not an image file");
                    return;
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            //check if image is already in album
            for(Album a : LoginViewController.currentUser.getAlbums()){
                if(a.getName().equals(albumName)){
                    for(Photo photo : a.getPhotos()){
                        if(photo.getName().equals(select.getName())){
                            //duplicate photo
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error");
                            alert.setContentText("Same photo name, can't add");
                            alert.showAndWait();
                            return;
                        }

                    }
                }
            }

            //now add the photo to the album and UI
            try {
                Stage s = (Stage)photoList.getScene().getWindow();
                String albumName = s.getTitle();
                Photo p = new Photo(select.getName(), c, select.getPath());

                for(Album a : LoginViewController.currentUser.getAlbums()){
                    if(a.getName().equals(albumName)){
                        a.getPhotos().add(p);
                        a.findMin();
                        a.findMax();
                        for(User u : AdminViewController.users){
                            if(u.getUserName().equals(LoginViewController.currentUser.getUserName())){
                                u.setAlbums(LoginViewController.currentUser.getAlbums());
                                LoginViewController.currentUser = u;
                                break;
                            }
                        }
                        break;
                    }
                }



                //displaying on UI
                FileInputStream f = new FileInputStream(select.getPath());
                Label l = new Label();
                Image i = new Image(f);
                ImageView iv = new ImageView(i);
                iv.setId(p.getName());
                iv.setFitHeight(200);
                iv.setFitWidth(200);
                l.setGraphic(iv);

                l.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        photoFunction(p, l);
                    }
                });
                photoList.getChildren().add(l);

                //serialize


                try {
                    AdminViewController.writeUser(AdminViewController.users);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            //System.out.println("exit photo select");
            return;
        }

    }

    /**
     * to remove a photo
     * @param p photo to be removed
     * @param l label that photo to be removed is in
     */
    @FXML
    public void removePhoto(Photo p, Label l){
        //remove photo from list
        //System.out.println("removign photo");
        for(Album a : LoginViewController.currentUser.getAlbums()){
            if(a.getName().equals(albumName)){
                for(Photo photo : a.getPhotos()){
                    if(photo.getName().equals(p.getName())){
                        //found the photo in the list, remove it
                        a.getPhotos().remove(photo);
                        a.findMax();
                        a.findMin();
                        //LoginViewController.currentUser.setAlbums(a);
                        break;
                    }
                }
            }
        }

        //update user list
        for(User u : AdminViewController.users){
            if(u.getUserName().equals(LoginViewController.currentUser.getUserName())){
                u.setAlbums(LoginViewController.currentUser.getAlbums());
                break;
            }
        }

        //update UI
        photoList.getChildren().remove(l);

        //serialize

        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * to copy or move a photo to another album
     * @param p photo to move or copy
     * @param s instruction to copy or move
     */
    public void copy_move_Photo(Photo p, String s){
        try {
            Stage clo = (Stage) tag.getScene().getWindow();
            clo.close();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/AlbumListView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            AlbumListViewController al = fxmlLoader.getController();
            al.setBool(p,s,albumName);
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Album List");
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * edit a tag
     * @param p Photo to be edited
     */
    public void editTag(Photo p){
        try {
            Stage clo = (Stage) tag.getScene().getWindow();
            clo.close();


            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/TagView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            TagViewController tv = fxmlLoader.getController();
            tv.setPhoto(p);
            tv.setAlbumName(albumName);
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Tag");
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * slide show view
     */
    @FXML
    public void slideshow(){

        //check if photo list is empty
        for(Album a : LoginViewController.currentUser.getAlbums()) {
            if(a.getName().equals(albumName)) {
                if(a.getPhotos().isEmpty()){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("No photos in album for slideshow");
                    alert.showAndWait();
                    return;
                }

            }

        }

        try {
            Stage clo = (Stage) tag.getScene().getWindow();
            clo.close();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/SlideShowView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            SlideShowViewController sv = fxmlLoader.getController();
            for(Album a : LoginViewController.currentUser.getAlbums()) {
                if(a.getName().equals(albumName)) {
                    sv.setPhotoList(a.getPhotos());
                    break;
                }

            }
            sv.setAlbumName(albumName);
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Tag");
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

   /* @FXML
    public void search(){
        try {
            Stage clo = (Stage) tag.getScene().getWindow();
            clo.close();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/SearchView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            SearchViewController sv = fxmlLoader.getController();
            sv.setAlbumName(albumName);
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Search");
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }*/

    /**
     * To view 1 individual photo
     * @param p photo to be viewed
     */
    @FXML
    public void displayPhoto(Photo p){
        try {
            Stage clo = (Stage) tag.getScene().getWindow();
            clo.close();


            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/IndividualPhotoDisplayView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            IndividualPhotoDisplayViewController iv = fxmlLoader.getController();
            iv.setPhoto(p);
            iv.setAlbumName(albumName);
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Individual Photo Display");
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set a caption
     * @param p Photo to caption
     */
    public void setCaption(Photo p){
        TextInputDialog tagD = new TextInputDialog("Enter caption");
        tagD.setTitle("Caption/Recaption");
        tagD.setHeaderText("Caption");
        tagD.setContentText("Enter caption");


        Optional<String> k = tagD.showAndWait();
        String newCaption = null;

        if (k.isPresent()){
            p.setCaption(k.get());
        } else {
            Alert er = new Alert(Alert.AlertType.ERROR);
            er.setTitle("Must enter something, try again");
            er.show();
            return;
        }

        photoList.getChildren().clear();
        populatePhotos(albumName);

        //serialize
        LoginViewController.currentUser.getAlbum(albumName).setPhoto(p);
        try {
            AdminViewController.writeUser(AdminViewController.users);
        }catch (Exception e){
            e.printStackTrace();
        }




    }


    /**
     * Populate view with photo
     * @param albumName album name photo is in
     */
    //given the album name, photos are displayed on the UI
    public void populatePhotos(String albumName){
        //System.out.println("Populating photos");
        for(Album a : LoginViewController.currentUser.getAlbums()){
           // System.out.println(a.getName() +","+albumName);
            if(a.getName().equals(albumName)){

                if(a.getPhotos().isEmpty()){
                   // System.out.println("empty album");
                    return;
                }

                for(Photo p : a.getPhotos()){
                    try {
                        //System.out.println("found it");
                        FileInputStream f = new FileInputStream(p.getPath());
                        String cap = p.getCaption();
                        Label l = new Label(cap);


                        Image i = new Image(f);
                        ImageView iv = new ImageView(i);
                        iv.setId(p.getName());
                        iv.setFitHeight(200);
                        iv.setFitWidth(200);
                        l.setGraphic(iv);

                        l.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                photoFunction(p, l);
                            }
                        });
                        photoList.getChildren().add(l);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
                break;
            }
        }
    }

    /**
     * helper function to see what method to do
     * @param p Photo thats being chosen
     * @param l Label that photos in
     */
    public void photoFunction(Photo p, Label l){
        if(rem){
            removePhoto(p, l);
        }else if(cap){
            setCaption(p);
        }else if(disp){
            displayPhoto(p);
        }else if(tagging){
            //load tag edit scene
            editTag(p);
        }else if(copying){
            copy_move_Photo(p, "copy");
        }else if(moving){
            copy_move_Photo(p, "move");
        } else {
            return;
        }

        if(!rem && !cap) {
            Stage clo = (Stage) remove.getScene().getWindow();
            clo.close();
        }
    }

    /**
     * go back to previous view
     */
    @FXML public void back(){
        Stage stage = (Stage) back.getScene().getWindow();
        stage.close();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/AlbumView.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stages = new Stage();
            stages.setScene(new Scene(root1));
            stages.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }


}
