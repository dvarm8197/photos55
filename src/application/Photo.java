/**
 * application contains main data structures and the driver
 */
package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Photo class
 * @author Adit Patel and Daniel Varma
 */
public class Photo implements Serializable {
    String name;
    ArrayList<Tag> tags ;
    Calendar date;
    String path;
    ArrayList<String> tagTypes;
    String caption;

    /**
     *
     * @param s Name of photo
     * @param c Date of photo
     * @param p Path name for photo
     */
    public Photo(String s, Calendar c, String p){
        name = s;
        tags = new ArrayList<Tag>();
        date = c;
        path = p;
        tagTypes = new ArrayList<>();
        tagTypes.add("Add new tag type");
        tagTypes.add("Location");
        tagTypes.add("People");
    }

    /**
     *
     * @return name of photo
     */
    public String getName(){return name;}


    /**
     *
     * @return tags for the photo
     */
    public ArrayList<Tag> getTags() {
        return tags;
    }

    /**
     *
     * @return date of the photo
     */
    public Calendar getDate() {
        return date;
    }

    /**
     *
     * @return path of photo
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param t Tag to add to photo
     */
    public void addTag(Tag t){
        tags.add(t);
    }

    public void addTagType(String s){
        tagTypes.add(s);
    }

    public ArrayList<String> getTagTypes(){ return tagTypes;}

    /**
     *
     * @param t list of tags
     */
    public void setTags(ArrayList<Tag> t){
        tags = t;
    }

    /**
     *
     * @param s new caption
     */
    public void setCaption(String s){caption = s;}

    /**
     *
     * @return caption of photo
     */
    public String getCaption(){return caption;}



}
