package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Album contains album information
 * @author Adit Patel and Daniel Varma
 */
public class Album implements Serializable {
    String name;
    int numPhotos;
    ArrayList<Photo> photos;
    Calendar minDate;
    Calendar maxDate;

    /**
     *
     * @param n Album name
     */
    public Album(String n){
        name = n;
        numPhotos = 0;
        photos = new ArrayList<Photo>();
        minDate = Calendar.getInstance();
        maxDate = Calendar.getInstance();
    }

    /**
     *
     * @return name of album
     */
    public String getName(){return name;}

    /**
     *
     * @return list of photos in album
     */
    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    /**
     *
     * @param p list of photos
     */
    public void setPhotos(ArrayList<Photo> p){
        photos = p;
    }

    /**
     *
     * @param photo 1 photo to set
     */
    public void setPhoto(Photo photo){
        int i = 0;
        for (Photo p : photos){
            if (p.getName().equals(photo.getName())){
                photos.set(i, photo);
                break;
            }
            i++;
        }
    }

    /**
     *
     * @param n new name
     */
    public void setName(String n){
        name = n;
    }

    /**
     * to get the oldest photo in album
     */
    public void findMin(){
       // minDate = null;
        if(photos.size() == 0){
            return;
        }
        if(photos.size() == 1){
            minDate = photos.get(0).getDate();
            return;
        }else {
            minDate = photos.get(0).getDate();
            for (Photo p : photos) {
                if (p.getDate().before(minDate)) {
                    minDate = p.getDate();
                }
            }
        }
    }

    /**
     *
     * @return the oldest photo
     */
    public Calendar getMinDate(){
        if(photos.size() == 0){
            return null;
        }
        return minDate;
    }


    /**
     * to get the newest photo in album
     */
    public void findMax(){
        //maxDate = null;
        if(photos.size() == 0 ){
            return;
        }
        if(photos.size() == 1){
            maxDate = photos.get(0).getDate();
            return;
        }else {
            maxDate = photos.get(0).getDate();
            for (Photo p : photos) {
                if (p.getDate().after(maxDate)) {
                    maxDate = p.getDate();
                }
            }
        }
    }

    /**
     *
     * @return the newest photo
     */
    public Calendar getMaxDate(){
        if(photos.size() == 0){
            return null;
        }
        return maxDate;
    }
}
