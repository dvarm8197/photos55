package application;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User class
 * @author Adit Patel and Daniel Varma
 */
public class User implements Serializable {
    String userName;
    ArrayList<Album> albums;
    ArrayList<String> tagTypes;

    /**
     *
     * @param name name of user
     */
    public User(String name){
        userName = name;
        albums = new ArrayList<Album>();
        tagTypes = new ArrayList<>();
        tagTypes.add("Add new tag type");
        tagTypes.add("Location");
        tagTypes.add("People");
    }

    /**
     *
     * @return tag types for this user
     */
   public  ArrayList<String> getTagTypes() { return tagTypes;}

    /**
     *
     * @param s new tag type
     */
   public void addTagType(String s){
        tagTypes.add(s);
   }

    /**
     *
     * @return name of user
     */
    public String getUserName (){return userName;}

    /**
     *
     * @return list of albums for user
     */
    public ArrayList<Album> getAlbums(){return albums;}

    /**
     *
     * @param a list of albums
     */
    public void setAlbums(ArrayList<Album> a){albums = a;}

    /**
     *
     * @param name album name
     * @return Album object
     */
    public Album getAlbum(String name){
        for (Album i: albums){
            if (name.equals(i.getName())){
                return i;
            }
        }
        return null;
    }
}
