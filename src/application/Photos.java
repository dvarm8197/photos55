package application;

import controller.AdminViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Photos is the driver that starts the application
 * @author Adit Patel and Daniel Varma
 *
 */
public class Photos extends Application {
    Boolean first = true;
    @Override
    public void start(Stage primaryStage) throws Exception{
        Boolean trip = false;

        File check = new File(System.getProperty("user.dir") + "/data/users.ser");

        if (!check.exists()){
            //System.out.println("first");
            User stock = new User("stock");
            Album s = new Album("stock");
            ArrayList<Album> al = new ArrayList();
            al.add(s);

            Calendar c = Calendar.getInstance();

            Photo p1 = new Photo("blur", c, System.getProperty("user.dir") + "/src/stockPhoto/blur.jpg");
            Photo p2 = new Photo("boat", c, System.getProperty("user.dir") + "/src/stockPhoto/boat.jpg");
            Photo p3 = new Photo("america", c, System.getProperty("user.dir") + "/src/stockPhoto/america.jpg");
            Photo p4 = new Photo("cat", c, System.getProperty("user.dir") + "/src/stockPhoto/cat.jpg");
            Photo p5 = new Photo("fox", c, System.getProperty("user.dir") + "/src/stockPhoto/fox.jpeg");
            Photo p6 = new Photo("blur", c, System.getProperty("user.dir") + "/src/stockPhoto/mario.jpg");

            ArrayList<Photo> pl = new ArrayList<>();
            pl.add(p1);
            pl.add(p2);
            pl.add(p3);
            pl.add(p4);
            pl.add(p5);
            pl.add(p6);


            al.get(0).setPhotos(pl);
            stock.setAlbums(al);

            AdminViewController.users.add(stock);
            AdminViewController.writeUser(AdminViewController.users);

            Parent root = FXMLLoader.load(getClass().getResource("../view/LoginView.fxml"));
            primaryStage.setTitle("Hello World");
            primaryStage.setScene(new Scene(root, 300, 275));
            primaryStage.show();

            first = false;
        } else {
            //System.out.println("second");
            Parent root = FXMLLoader.load(getClass().getResource("../view/LoginView.fxml"));
            primaryStage.setTitle("Hello World");
            primaryStage.setScene(new Scene(root, 300, 275));
            primaryStage.show();
        }


    }
    //test push

    public static void main(String[] args) {

        launch(args);
    }
}
