package application;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Tag class
 * @author Adit Patel and Daniel Varma
 */
public class Tag implements Serializable {

    String tagName;
    String tagValue;


    static ArrayList<String> preLoadedTags = new ArrayList<String>();

    public void generateDefaultTag(){
        preLoadedTags.add("Location");
        preLoadedTags.add("Person");
    }

    /**
     *
     * @return name of tag
     */
    public String getTagName(){return tagName;}


    /**
     *
     * @return value of the tag
     */
    public String getTagValue(){return tagValue;}

    /**
     *
     * @param s name of tag
     */
    public void setTagName(String s){ tagName = s;}

    /**
     *
     * @param s value of tag
     */
    public void setTagValue(String s) { tagValue = s;}

}
